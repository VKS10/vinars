package write

import (
	"encoding/json"
	"../../service"
	"fmt"
	"strconv"
	helper "../../../lib/core/helper"
	response "../../../lib/core/response"
)
type registerSuccess struct {
	UserId string
}
func (c *WriteController) UserRegister () {
	var u service.User
	err := json.NewDecoder(c.Request.Body).Decode(&u)
	if err != nil {
		c.Result, c.StatusCode = response.ErrorCodeResponse("INVALID_INPUT")
		return
	}
	u.RegisterIpAddress = helper.ReadUserIP(c.Request)

	err, userId := u.AddUser(c.DB)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}
	if userId != 0 {
		c.Result = response.ResponseWithData(formatResponseRegister(strconv.FormatInt(userId, 10)), "Registered successfully")
		return
	}
}

func (c *WriteController) AgentRegister () {

}

func (c *WriteController) DoctorRegister () {

}


func formatResponseRegister(result string) []interface{} {
	pRes := registerSuccess{
		UserId: result,
	}
	var data []interface{}
	return append(data, pRes)
}
