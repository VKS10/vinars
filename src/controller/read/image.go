package read
import (
	"../../service"
	"fmt"
	response "../../../lib/core/response"
)

type imgSuccessWithData struct {
	Images []service.Image
}
func (c *ReadController) LabImages (labId int64) {
	var i service.Image
	//check permission
	//@todo

	result, err := i.GetAllImagesByLabId(c.DB, labId)

	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	data := formatImageResponse(result)
	c.Result = response.ResponseWithData(data, "")
	return
}

func (c *ReadController) ProductImages (productId int64) {
	var i service.Image
	//check permission
	//@todo

	result, err := i.GetAllImagesByProductId(c.DB, productId)

	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	data := formatImageResponse(result)
	c.Result = response.ResponseWithData(data, "")
	return
}

func formatImageResponse(result []service.Image) []interface{} {
	pRes := imgSuccessWithData{
		Images: result,
	}
	var data []interface{}
	return append(data, pRes)
}
