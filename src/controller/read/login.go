package read
import (
	response "../../../lib/core/response"
	authToken "../../../lib/core/token"
	"fmt"
)

func (c *ReadController) Logout () {
	if  c.UserId == 0 {
		c.Result, _ = response.ErrorCodeResponse("INVALID_AUTHORIZATION")
		return
	}
	err := authToken.DeleteToken(c.DB, c.Configuration, c.Token)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, _ = response.ErrorResponse(err)
		return
	}
	c.Result = response.ResponseWithoutData("SUCCESS", "Logged-out")
	return
}
