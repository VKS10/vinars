package repository

import (
	"../../lib/core/database"
	"strconv"
)

func InsertUserAddress (
	db *database.Database,
	data map[string]string,
) (addressId int64, err error) {
	addressId, err = db.Insert("user_address",  data)
	if err != nil {
		return 0, err
	}

	return addressId, nil
}

func UpdateUserAddress (
	db *database.Database,
	addressId int64,
	data map[string]string,
) ( err error) {
	var condition = map[string]string {
		"id" : strconv.FormatInt(addressId, 10),
	}
	_, err = db.Update("user_address",  data, condition)
	if err != nil  {
		return err
	}

	return nil
}

func DeleteUserAddress(db *database.Database,
	addressId int64,) ( err error) {
	var condition = map[string]string {
		"id" : strconv.FormatInt(addressId, 10),
	}
	_, err = db.Delete("user_address", condition)
	if err != nil  {
		return err
	}

	return nil
}

func GetUserAddressById(db *database.Database, addressId int64) (userAddress map[string]string, err error) {
	var id, name, adty, address_line_1, address_line_2, address_line_3, city, zip, state, country string
	var user_id, add_datetime, updated_dateTime, added_by, status string
	var condition = map[string]string {
		"id" : strconv.FormatInt(addressId, 10),
	}
	var fields = []string{
		"id",
		"name",
		"adty",
		"address_line_1",
		"address_line_2",
		"address_line_3",
		"city",
		"zip",
		"state",
		"country",
		"user_id",
		"add_datetime",
		"updated_dateTime",
		"added_by",
		"status",
	}

	var orderBy = []string{"id"}
	rows, err := db.Select("user_address", fields , condition, orderBy, 1)
	if err != nil {
		return userAddress, err
	}

	if rows.Next() {
		err := rows.Scan(&id, &name, &adty, &address_line_1, &address_line_2, &address_line_3, &city, &zip, &state,
			&country, &user_id, &add_datetime, &updated_dateTime, &added_by, &status)
		if err == nil {
			userAddress = map[string]string{
				"id":               id,
				"name":             name,
				"adty":             adty,
				"address_line_1":   address_line_1,
				"address_line_2":   address_line_2,
				"address_line_3":   address_line_3,
				"city":             city,
				"zip":              zip,
				"state":            state,
				"country":          country,
				"user_id":          user_id,
				"add_datetime":     add_datetime,
				"updated_dateTime": updated_dateTime,
				"added_by":         added_by,
				"status":           status,
			}
			return userAddress, nil
		}
	}

	return userAddress, err
}

func GetUserAddressByUserId(db *database.Database, userId int64) (userAddressList []map[string]string, err error) {
	var id, name, adty, address_line_1, address_line_2, address_line_3, city, zip, state, country string
	var user_id, add_datetime, updated_dateTime, added_by, status string
	var condition = map[string]string {
		"user_id" : strconv.FormatInt(userId, 10),
	}
	var fields = []string{
			"id",
			"name",
			"adty",
			"address_line_1",
			"address_line_2",
			"address_line_3",
			"city",
			"zip",
			"state",
			"country",
			"user_id",
			"add_datetime",
			"updated_dateTime",
			"added_by",
			"status",
	}

	var orderBy = []string{"id"}
	rows, err := db.Select("user_address", fields , condition, orderBy, 0)
	if err != nil {
		return userAddressList, err
	}
	var userAddress map[string]string
	for rows.Next() {
		err := rows.Scan(&id, &name, &adty, &address_line_1, &address_line_2, &address_line_3, &city, &zip, &state,
			&country, &user_id, &add_datetime, &updated_dateTime, &added_by, &status)
		if err == nil {
			userAddress = map[string]string{
				"id":               id,
				"name":             name,
				"adty":             adty,
				"address_line_1":   address_line_1,
				"address_line_2":   address_line_2,
				"address_line_3":   address_line_3,
				"city":             city,
				"zip":              zip,
				"state":            state,
				"country":          country,
				"user_id":          user_id,
				"add_datetime":     add_datetime,
				"updated_dateTime": updated_dateTime,
				"added_by":         added_by,
				"status":           status,
			}

			userAddressList = append(userAddressList, userAddress)
		}
	}

	return userAddressList, nil
}