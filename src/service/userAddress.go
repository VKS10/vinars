package service

import (
	"../../lib/core/database"
	"../../lib/core/errors"
	"../repository"
	"fmt"
	"strconv"
	"time"
)

type UserAddress struct {
	Id int64
	UserId int64
	Adty string
	Name string
	AddressLine1 string
	AddressLine2 string
	AddressLine3 string
	City string
	Zip string
	State string
	Country string
	AddedDateTime string
	UpdateDateTime string
	AddedBy int64
}

func (ua *UserAddress) AddUserAddress(db *database.Database) (err error, addressId int64){
	//validate input
	if len(ua.Name) < 1 {
		return errors.CustomErrors("NAME_EMPTY"),0
	}
	if len(ua.Adty) < 1 {
		return errors.CustomErrors("ADTY_EMPTY"),0
	}
	if len(ua.AddressLine1) < 1 {
		return errors.CustomErrors("ADDRESS1_EMPTY"),0
	}
	if len(ua.City) < 1 {
		return errors.CustomErrors("CITY_EMPTY"),0
	}
	if len(ua.Country) < 1 {
		return errors.CustomErrors("COUNTRY_EMPTY"),0
	}

	ua.AddedDateTime  = time.Now().Format("2006-01-02 15:04:05")
	ua.UpdateDateTime  = time.Now().Format("2006-01-02 15:04:05")

	//call db query
	var data = map[string]string {
		"name": ua.Name,
		"adty": ua.Adty,
		"address_line_1": ua.AddressLine1,
		"address_line_2": ua.AddressLine2,
		"address_line_3": ua.AddressLine3,
		"city": ua.City,
		"zip": ua.Zip,
		"state": ua.State,
		"country": ua.Country,
		"user_id": strconv.FormatInt(ua.UserId, 10),
		"add_datetime": ua.AddedDateTime,
		"updated_dateTime": ua.UpdateDateTime,
		"added_by": strconv.FormatInt(ua.AddedBy, 10),
	}

	addressId, err = repository.InsertUserAddress(db, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR"),0
	}

	return nil, addressId
}

func (ua *UserAddress) UpdateUserAddress(db *database.Database) (err error) {
	//validate input
	if len(ua.Name) < 1 {
		return errors.CustomErrors("NAME_EMPTY")
	}
	if len(ua.Adty) < 1 {
		return errors.CustomErrors("ADTY_EMPTY")
	}
	if len(ua.AddressLine1) < 1 {
		return errors.CustomErrors("ADDRESS1_EMPTY")
	}
	if len(ua.City) < 1 {
		return errors.CustomErrors("CITY_EMPTY")
	}
	if len(ua.Country) < 1 {
		return errors.CustomErrors("COUNTRY_EMPTY")
	}
	ua.UpdateDateTime  = time.Now().Format("2006-01-02 15:04:05")
	//call db query
	var data = map[string]string {
		"name": ua.Name,
		"adty": ua.Adty,
		"address_line_1": ua.AddressLine1,
		"address_line_2": ua.AddressLine2,
		"address_line_3": ua.AddressLine3,
		"city": ua.City,
		"zip": ua.Zip,
		"state": ua.State,
		"country": ua.Country,
		"updated_dateTime": ua.UpdateDateTime,
		"added_by": strconv.FormatInt(ua.AddedBy, 10),
	}

	err = repository.UpdateUserAddress(db, ua.Id, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}

	return nil
}

func (ua *UserAddress) DeleteUserAddress(db *database.Database) (err error) {

	err = repository.DeleteUserAddress(db, ua.Id)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}

	return nil
}


func (ua *UserAddress) ActivateUserAddress(db *database.Database) (err error) {
	ua.UpdateDateTime  = time.Now().Format("2006-01-02 15:04:05")
	//call db query
	var data = map[string]string {
		"status": "1",
		"updated_dateTime": ua.UpdateDateTime,
		"added_by": strconv.FormatInt(ua.AddedBy, 10),
	}

	err = repository.UpdateUserAddress(db, ua.Id, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}

	return nil
}

func (ua *UserAddress) DeactivateUserAddress(db *database.Database) (err error) {
	ua.UpdateDateTime  = time.Now().Format("2006-01-02 15:04:05")
	//call db query
	var data = map[string]string {
		"status": "0",
		"updated_dateTime": ua.UpdateDateTime,
		"added_by": strconv.FormatInt(ua.AddedBy, 10),
	}

	err = repository.UpdateUserAddress(db, ua.Id, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}

	return nil
}

func (ua *UserAddress) GetUserAddressById(db *database.Database, addressId int64) (result []UserAddress, err error) {

	data, err := repository.GetUserAddressById(db, addressId)
	if err != nil {
		//log error
		fmt.Println(err)
		return result, errors.CustomErrors("INTERNAL_ERROR")
	}

	if len(data) == 0 {
		return result, errors.CustomErrors("NOT_FOUND")
	}

	var userAddress = UserAddress{}
	userAddress.Id, _ = strconv.ParseInt(data["id"], 10, 64)
	userAddress.UserId, _ = strconv.ParseInt(data["user_id"], 10, 64)
	userAddress.AddedBy, _ = strconv.ParseInt(data["added_by"], 10, 64)
	userAddress.AddedDateTime = data["add_datetime"]
	userAddress.UpdateDateTime = data["updated_dateTime"]
	userAddress.Adty = data["adty"]
	userAddress.Name = data["name"]
	userAddress.AddressLine1 = data["address_line_1"]
	userAddress.AddressLine2 = data["address_line_2"]
	userAddress.AddressLine3 = data["address_line_3"]
	userAddress.City = data["city"]
	userAddress.Zip = data["zip"]
	userAddress.State = data["state"]
	userAddress.Country = data["country"]

	result = append(result, userAddress)
	return result, err
}

func (ua *UserAddress) GetAllUserAddressForUser(db *database.Database, userId int64) (result []UserAddress, err error) {
	addressData, err := repository.GetUserAddressByUserId(db, userId)
	if err != nil {
		//log error
		fmt.Println(err)
		return result, errors.CustomErrors("INTERNAL_ERROR")
	}

	for _, data := range addressData {
		var userAddress = UserAddress{}
		userAddress.Id, _ = strconv.ParseInt(data["id"], 10, 64)
		userAddress.UserId, _ = strconv.ParseInt(data["user_id"], 10, 64)
		userAddress.AddedBy, _ = strconv.ParseInt(data["added_by"], 10, 64)
		userAddress.AddedDateTime = data["add_datetime"]
		userAddress.UpdateDateTime = data["updated_dateTime"]
		userAddress.Adty = data["adty"]
		userAddress.Name = data["name"]
		userAddress.AddressLine1 = data["address_line_1"]
		userAddress.AddressLine2 = data["address_line_2"]
		userAddress.AddressLine3 = data["address_line_3"]
		userAddress.City = data["city"]
		userAddress.Zip = data["zip"]
		userAddress.State = data["state"]
		userAddress.Country = data["country"]

		result = append(result, userAddress)
	}

	return result, err
}