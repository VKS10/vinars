package service

import (
	cnf "../../configuration"
	"gopkg.in/gomail.v2"
	"strconv"
	"fmt"
)

func SendEmail(
	conf *cnf.Configuration,
	from string,
	to map[string]string,
	cc map[string]string,
	bcc map[string]string,
	subject string,
	message string,
	isHTML bool,
	files map[int]string) (err error) {
	m := gomail.NewMessage()
	m.SetHeader("From", from)
	for _, toEmail := range to {
		m.SetHeader("To", toEmail)
	}
	for _, ccEmail := range cc {
		m.SetHeader("Cc", ccEmail)
	}
	for _, bccEmail := range bcc {
		m.SetHeader("Bcc", bccEmail)
	}

	m.SetHeader("Subject", subject)

	if isHTML {
		m.SetBody("text/html", message)
	} else {
		m.SetBody("text/plain", message)
	}

	for _, file := range files {
		m.Attach(file)
	}

	//go get gopkg.in/gomail.v2
	d := gomail.NewPlainDialer(conf.SMTP_Server, conf.SMTP_Port, conf.SMTP_User, conf.SMTP_Pass)

	// Send the email
	if err := d.DialAndSend(m); err != nil {
		fmt.Println(err);
		return err
	}

	return nil
}

func NewConsultationEmail(conf *cnf.Configuration, c *Consultation)  {
	//send new consultation email

	msg := "Hello <b> Manager</b>, <br> Please check new consultation request " +
		" from: " + c.Name +
		" <br>Mobile: " + c.Mobile +
		" <br>email: " + c.Email +
		" <br>comment: " + c.Comment +
		" <br> " +
		"Request Id: "+ strconv.FormatInt(c.Id, 10)
	if c.Prescription != "" {
		msg = msg + " <br> Prescription File: " + conf.Web_URL + "/img/prescription/" + c.Prescription
	}

	msg = msg + "<br><br><i>Your's</i> <br> MyPathoLab team!"

	subject := "New consultation request on MyPathoLab:" + c.City

	toEmail :=make(map[string]string)
	ccEmail :=make(map[string]string)
	bccEmail :=make(map[string]string)
	files :=make(map[int]string)
	if c.AgencyId != "0" {
		toEmail[c.AgencyDetail.Name] = c.AgencyDetail.Email
		ccEmail["MyPathoLab"] = conf.ADMIN_EMAIL_GROUP
	} else {
		toEmail["MyPathoLab"] = conf.ADMIN_EMAIL_GROUP
	}

	err := SendEmail(conf,  "ishtan.mylab@gmail.com", toEmail, ccEmail, bccEmail, subject, msg, true, files)
	if err != nil  {
		fmt.Println(err)
	}
}

func NewOrderEmailToAgency(conf *cnf.Configuration, o *Orders)  {
	//send new Order email
	msg := "Hello <b> Manager</b>, <br> Please check new Order for you " +
		"Order Id: "+ strconv.FormatInt(o.Id, 10)+
		" from: " + o.UserDetail.Name +
		" <br>Mobile: " + o.PatientDetail.Mobile +
		" <br>email: " + o.PatientDetail.Email +
		" <br>comment: " + o.Detail +
		" <br> "
	if o.PrescriptionImg != "" {
		msg = msg + " <br> Prescription: " + conf.Web_URL + "/img/" + o.PrescriptionImg
	}

	msg = msg + "<br><br><i>Your's</i> <br> MyPathoLab team!"

	subject := "[" + o.AgencyDetail.Name +"]New Order on MyPathoLab Id [" + strconv.FormatInt(o.Id, 10) +"]"
	toEmail :=make(map[string]string)
	ccEmail :=make(map[string]string)
	bccEmail :=make(map[string]string)
	files :=make(map[int]string)
	toEmail[o.AgencyDetail.Name] = o.AgencyDetail.Email
	ccEmail["MyPathoLab"] = conf.ADMIN_EMAIL_GROUP

	err := SendEmail(conf,  "ishtan.mylab@gmail.com", toEmail, ccEmail, bccEmail, subject, msg, true, files)
	if err != nil  {
		fmt.Println(err)
	}

	return
}

func NewOrderEmailToUser(conf *cnf.Configuration, o *Orders)  {
	//send new Order email
	msg := "Hello <b> "+o.UserDetail.Name+"</b>, <br> Thank you for your Order" +
		"Order Id: "+ strconv.FormatInt(o.Id, 10)+
		" Our Agent will contact you soon. "

	msg = msg + "<br><br><i>Your's</i> <br> MyPathoLab team!"

	subject := "New Order on MyPathoLab Id [" + strconv.FormatInt(o.Id, 10) +"]"
	toEmail :=make(map[string]string)
	ccEmail :=make(map[string]string)
	bccEmail :=make(map[string]string)
	files :=make(map[int]string)
	toEmail[o.UserDetail.Name] = o.UserDetail.Email
	ccEmail["MyPathoLab"] = conf.ADMIN_EMAIL_GROUP

	err := SendEmail(conf,  "ishtan.mylab@gmail.com", toEmail, ccEmail, bccEmail, subject, msg, true, files)
	if err != nil  {
		fmt.Println(err)
	}
	return
}
