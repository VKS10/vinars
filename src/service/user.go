package service

import (
	cnf "../../configuration"
	"../../lib/core/database"
	"../../lib/core/errors"
	helper "../../lib/core/helper"
	authToken "../../lib/core/token"
	"../../lib/core/validator"
	"../repository"
	"fmt"
	"github.com/google/uuid"
	"gopkg.in/gomail.v2"
	"strconv"
	"time"
)
type Password struct {
	Email string
	HashCode string
	OldPassword string
	Password string
}

type User struct {
	Id int64
	Email string
	Name string
	Password string
	Status int
	RegisterDateTime string
	UpdateDateTime string
	RegisterIpAddress string
	HashCode string
	AgencyId int64
	AgencyCity string
	UserType string
}

func (u *User) AddUser(db *database.Database, conf *cnf.Configuration) (err error, userId int64){
	//validate input
	if len(u.Email) < 1 {
		return errors.CustomErrors("EMAIL_EMPTY"),0
	}
	if len(u.Password) < 1 {
		return errors.CustomErrors("PASSWORD_EMPTY"),0
	}
	if len(u.Name) < 1 {
		return errors.CustomErrors("NAME_EMPTY"),0
	}
	if !validator.IsValidEmail(u.Email) {
		return errors.CustomErrors("EMAIL_INVALID"),0
	}

	//check if email already exist
	ok, err := repository.IsUserExistByEmail(db, u.Email)
	if ok == true {
		return errors.CustomErrors("EMAIL_DUPLICATE"),0
	}
	//prepare data set
	u.Status = 1
	u.Password = helper.GenerateHashedPassword(u.Password)
	u.RegisterDateTime  = time.Now().Format("2006-01-02 15:04:05")
	//call db query

	uid := uuid.New()
	u.HashCode = uid.String();
	userId, err = repository.InsertUser(db, u.Email, u.Name, u.Password, u.Status, u.RegisterIpAddress, u.HashCode)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR"),0
	}

	//send confirmation link
	var link = conf.Web_URL + "/confirmUser/" + u.HashCode
	m := gomail .NewMessage()
	m.SetHeader("From", "ishtan.mylab@gmail.com")
	m.SetHeader("To", u.Email)
	m.SetAddressHeader("Bcc", "ishtan.mylab@gmail.com", "MyLab")
	m.SetHeader("Subject", "Confirm your registration with MyLab")
	m.SetBody("text/html", "Hello <b>" + u.Name +  "</b>, <br> Please confirm your email address for " +
		"your request to register with MyLab. Please click to the below link <br> <br> <a href='" + link +
		"'>" + link + "</a>  <br> <br> <i>Your's</i> <br> MyLab team!")
	//m.Attach("/home/Alex/lolcat.jpg")
	//go get gopkg.in/gomail.v2
	d := gomail.NewPlainDialer(conf.SMTP_Server, conf.SMTP_Port, conf.SMTP_User, conf.SMTP_Pass)

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}
	return nil, userId
}

func (u *User) Login(db *database.Database, configuration *cnf.Configuration, deviceName string, deviceToken  string) (err error, token string){
	//validate input
	if len(u.Email) < 1 {
		return errors.CustomErrors("EMAIL_EMPTY"),""
	}
	if len(u.Password) < 1 {
		return errors.CustomErrors("PASSWORD_EMPTY"),""
	}
	if !validator.IsValidEmail(u.Email) {
		return errors.CustomErrors("EMAIL_INVALID"),""
	}

	//fetch user if email exist
	userId, _, password, status, err := repository.GetUserByEmail(db, u.Email)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR"),""
	}
	if ! helper.VerifyHashedPassword(password, u.Password) {
		return errors.CustomErrors("INVALID_AUTHORIZATION"),""
	}
	if status != 1 {
		fmt.Println(status)
		return errors.CustomErrors("INACTIVE_USER"),""
	}
	token = authToken.GetNewTokenForUser(db, configuration, userId, u.Email)

	if deviceToken != "" && deviceName != ""{
		err = repository.ReplaceDeviceToken(db, userId, deviceName, deviceToken)
		println(err)
	}
	return nil, token
}

func (u *User) ConfirmUser(hash string, db *database.Database) (err error, userId int64) {
	userId, err = repository.GetUserByHash(db, hash)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR"),userId
	}

	if userId == 0 {
		return errors.CustomErrors("INVALID_HASH"),userId
	}

	u.UpdateDateTime  = time.Now().Format("2006-01-02 15:04:05")
	//call db query
	var data = map[string]string {
		"status": "1",
		"hashCode": "",
		"update_dateTime": u.UpdateDateTime,
	}

	err = repository.UpdateUser(db, userId, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR"), 0
	}

	return err, userId
}

func (u *User) AllUsers(
	db *database.Database,
) (result []User, err error) {

	userData, err := repository.AllUser(db)
	if err != nil {
		//log error
		fmt.Println(err)
		return result, errors.CustomErrors("INTERNAL_ERROR")
	}

	for _, data := range userData {
 		var user = User{}
		user.Id, _ = strconv.ParseInt(data["id"], 10, 64)
		user.Name =  data["name"]
		user.Email =  data["email"]
		user.Status, _ = strconv.Atoi(data["status"])
		user.RegisterDateTime =  data["register_datetime"]
		user.UpdateDateTime =  data["update_datetime"]
		user.RegisterIpAddress =  data["register_ip"]

		result = append(result, user)
	}

	return result, err
}


func (u *User) SearchUser(
	db *database.Database,
	name string,
	email string,
) (result []User, err error) {

	userData, err := repository.SearchUser(db, name, email)
	if err != nil {
		//log error
		fmt.Println(err)
		return result, errors.CustomErrors("INTERNAL_ERROR")
	}

	for _, data := range userData {
		var user = User{}
		user.Id, _ = strconv.ParseInt(data["id"], 10, 64)
		user.Name =  data["name"]
		user.Email =  data["email"]
		user.Status, _ = strconv.Atoi(data["status"])
		user.RegisterDateTime =  data["register_datetime"]
		user.UpdateDateTime =  data["update_datetime"]
		user.RegisterIpAddress =  data["register_ip"]

		result = append(result, user)
	}

	return result, err
}

func (p *Password) ResetPassword(
	db *database.Database,
	conf *cnf.Configuration,
) (err error) {
	//validate input
	if len(p.Email) < 1 {
		return errors.CustomErrors("EMAIL_EMPTY")
	}

	//fetch user if email exist
	userId, name, _, _, err := repository.GetUserByEmail(db, p.Email)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}
	if (userId == 0) {
		return errors.CustomErrors("INVALID_AUTHORIZATION")
	}
	uid := uuid.New()
	var hashCode = uid.String();
	var updateDateTime  = time.Now().Format("2006-01-02 15:04:05")
	var data = map[string]string {
		"hashCode": hashCode,
		"update_dateTime": updateDateTime,
	}

	err = repository.UpdateUser(db, userId, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}

	//send confirmation link
	var link = conf.Web_URL + "/resetPassword/" + hashCode
	m := gomail .NewMessage()
	m.SetHeader("From", "ishtan.mylab@gmail.com")
	m.SetHeader("To", p.Email)
	m.SetAddressHeader("Bcc", "ishtan.mylab@gmail.com", "MyLab")
	m.SetHeader("Subject", "Reset password request on MyLab")
	m.SetBody("text/html", "Hello <b>" + name +  "</b>, <br> You have requested password reset " +
		" for your account on MyLab. Please click to the below link to generate a new password<br> <br> <a href='" + link +
		"'>" + link + "</a>  <br>  if this is not requested by you then please ignore this and contact us. <br>" +
		"<br> <i>Your's</i> <br> MyLab team!")
	//m.Attach("/home/Alex/lolcat.jpg")
	//go get gopkg.in/gomail.v2
	d := gomail.NewPlainDialer(conf.SMTP_Server, conf.SMTP_Port, conf.SMTP_User, conf.SMTP_Pass)

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}
	return nil
}

func (p *Password) ChangePassword(
	db *database.Database,
	conf *cnf.Configuration,
	userId int64,
) (err error) {
	//validate input
	if len(p.OldPassword) < 1 && len(p.HashCode) < 1{
		return errors.CustomErrors("INVALID_AUTHORIZATION")
	}

	if len(p.Password) < 1{
		return errors.CustomErrors("PASSWORD_EMPTY")
	}
	var updateDateTime  = time.Now().Format("2006-01-02 15:04:05")
	var hashedPassword = helper.GenerateHashedPassword(p.Password)
	if len(p.HashCode) > 1{
		userId, err = repository.GetUserByHash(db, p.HashCode)
		if err != nil {
			//log error
			fmt.Println(err)
			return errors.CustomErrors("INTERNAL_ERROR")
		}

		if userId == 0 {
			return errors.CustomErrors("INVALID_HASH")
		}

	} else if len(p.OldPassword) > 1 && userId != 0 {
		//fetch user if  exist
		_, _, _, password, err := repository.GetUserById(db, userId)
		if err != nil {
			//log error
			fmt.Println(err)
			return errors.CustomErrors("INTERNAL_ERROR")
		}

		if !helper.VerifyHashedPassword(password, p.OldPassword) {
			return errors.CustomErrors("INVALID_AUTHORIZATION")
		}
	} else {
		return errors.CustomErrors("INVALID_AUTHORIZATION")
	}

	var data = map[string]string {
		"password": hashedPassword,
		"update_dateTime": updateDateTime,
		"hashCode": "",
	}
	err = repository.UpdateUser(db, userId, data)
	if err != nil {
		//log error
		fmt.Println(err)
		return errors.CustomErrors("INTERNAL_ERROR")
	}

	return nil
}

func GetUserById(db *database.Database, userId int64) (user User) {
	//fetch user if  exist
	email, name, status, _, err := repository.GetUserById(db, userId)
	if err != nil {
		//log error
		fmt.Println(err)
		return user
	}
	agencyId, agencyCity, _ := GetAgencyByUser(db, userId)

	user.Id = userId
	user.Name = name
	user.Email = email
	user.Status, _ = strconv.Atoi(status)

	user.AgencyId = agencyId
	user.AgencyCity = agencyCity
	user.UserType = GetUserGroupType(db, userId)

	return user
}


