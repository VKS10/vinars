package errors
import (
	"github.com/micro/go-micro/errors"
)
type CustomError struct{
	Id string `json:"id"`
	Code int32 `json:"code"`
	Detail string `json:"detail"`
	Status string `json:"status"`
}

var errorMsg = map[string]string{
	"ALREADY_LOGGED_IN"  : "Already logged-in",
	"INTERNAL_ERROR" : "Internal server error",

	"FILE_MAX_SIZE_LIMIT":  "File size limit exceed",

	"INVALID_INPUT" : "input param are not valid",
	"EMAIL_EMPTY": "Email is required",
	"EMAIL_INVALID": "Email is invalid",
	"EMAIL_DUPLICATE": "Email is already registered",
	"MOBILE_EMPTY": "Mobile number is required",
	"PASSWORD_EMPTY": "Password is required",
	"NAME_EMPTY": "Name is required",
	"FIRST_NAME_EMPTY": "First name is required",
	"LAST_NAME_EMPTY": "Last name is required",
	"GENDER_EMPTY": "Gender is required",
	"ADDRESS1_EMPTY": "Address Line 1 is required",
	"ADDRESS_EMPTY": "Address is required",
	"CITY_EMPTY": "City is required",
	"COUNTRY_EMPTY": "Country is required",
	"ADTY_EMPTY": "Address type is required",

	"COUPON_EMPTY": "Coupon code is required",
	"COUPON_VALUE_EMPTY": "Coupon value is required",
	"COUPON_NOT_FOUND": "Coupon code not found",
	"COUPON_IS_NOT_VALID": "Coupon code is not valid",
	"COUPON_DOES_NOT_APPLY_ON_THIS_AMOUNT": "Total order amount is less then minimum required amount for this  coupon",
	"COUPON_REACHED_MAX_USE_LIMIT" : "Coupon reached max use limit",
	"COUPON_DOES_NOT_BELONGS_TO_THIS_USER": "This  coun does not belongs to requested user",

	"IMG_URL_EMPTY": "Image url is empty",
	"PRODUCT_CONNECTION_EMPTY": "Product connection is empty",
	"LAB_CONNECTION_EMPTY": "Lab connection is empty",

	"LAB_NAME_EMPTY": "Lab  name is empty",

	"PATIENT_EMPTY": "Patient id  is required",
	"PRODUCT_ID_EMPTY": "Product Id is required",
	"PRODUCT_NAME_EMPTY": "Product name is required",
	"PRODUCT_QUANTITY_EMPTY": "Product quantity can not be  less than 1",
	"INVALID_APPOINTMENT_DATE":  "Invalid appointment date" ,
	"ORDER_ID_EMPTY": "Order Id is required",
	"ORDER_NOT_FOUND": "Order not found",
	"PRICE_EMPTY": "Product price is required",
	"AGENCY_EMPTY": "Agency can not be empty",
	"LAB_ID_EMPTY": "Lab id can not be  empty",


	"REPORT_NOT_FOUND": "Report not found",
	"REPORT_DETAIL_EMPTY" : "Report detail is required",
	"REPORT_FILE_EMPTY" : "Report file is required",
	//
	"INVALID_HASH": "Invalid hash code",
	"INVALID_AUTHORIZATION" : "Invalid Authorization",
	"NOT_FOUND": "Page not found",
	"INACTIVE_USER" : "User is not activated yet",
	"PERMISSION_DENIED" : "You are not allowed to perform this operation",

}
var errorCodes = map[string]int32{

	"ALREADY_LOGGED_IN": 200,
	"INTERNAL_ERROR" : 500,
	"FILE_MAX_SIZE_LIMIT": 500,

	"INVALID_INPUT": 400,
	"EMAIL_EMPTY": 400,
	"EMAIL_INVALID": 400,
	"EMAIL_DUPLICATE": 400,
	"MOBILE_EMPTY": 400,
	"PASSWORD_EMPTY": 400,
	"NAME_EMPTY": 400,
	"FIRST_NAME_EMPTY": 400,
	"LAST_NAME_EMPTY": 400,
	"GENDER_EMPTY": 400,
	"ADDRESS1_EMPTY": 400,
	"ADDRESS_EMPTY": 400,
	"CITY_EMPTY": 400,
	"COUNTRY_EMPTY": 400,
	"ADTY_EMPTY": 400,

	"COUPON_EMPTY": 400,
	"COUPON_VALUE_EMPTY": 400,
	"COUPON_NOT_FOUND": 400,
	"COUPON_IS_NOT_VALID": 400,
	"COUPON_DOES_NOT_APPLY_ON_THIS_AMOUNT": 400,
	"COUPON_REACHED_MAX_USE_LIMIT" : 400,
	"COUPON_DOES_NOT_BELONGS_TO_THIS_USER": 400,

	"IMG_URL_EMPTY": 400,
	"PRODUCT_CONNECTION_EMPTY": 400,
	"LAB_CONNECTION_EMPTY": 400,

	"LAB_NAME_EMPTY": 400,

	"PATIENT_EMPTY": 400,
	"PRODUCT_ID_EMPTY": 400,
	"PRODUCT_NAME_EMPTY": 400,
	"PRODUCT_QUANTITY_EMPTY": 400,
	"INVALID_APPOINTMENT_DATE":  400,
	"ORDER_ID_EMPTY": 400,
	"ORDER_NOT_FOUND": 400,
	"PRICE_EMPTY": 400,
	"AGENCY_EMPTY": 400,
	"LAB_ID_EMPTY": 400,

	"REPORT_NOT_FOUND": 404,
	"REPORT_DETAIL_EMPTY" : 400,
	"REPORT_FILE_EMPTY" : 400,
	//
	"INVALID_HASH": 401,
	"INVALID_AUTHORIZATION" : 401,
	"PERMISSION_DENIED" : 401,
	"INACTIVE_USER" : 402,
	"NOT_FOUND":404,
}

func CustomErrors(id string) error  {
	if _, ok := errorMsg[id]; ok {
		return errors.New(id, errorMsg[id], errorCodes[id])
	}

	return errors.New(id, errorMsg["INTERNAL_ERROR"], errorCodes["INTERNAL_ERROR"])
}
